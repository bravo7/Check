﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Check
{
    public class CheckFile
    {
        public List<string> Directories
        { get; set; }
        public string Name
        { get; set; }
        private int count;

        public CheckFile(string pathToFile)
        {
            Directories = new List<string>();
            this.count = 0;
            string[] tab = pathToFile.Split('\\');
            for (int i = 0; i < tab.Length; i++)
                if (i != tab.Length - 1)
                    this.Directories.Add(tab[i]);
                else
                    this.Name = tab[i];
        }

        public string getNextFolder()
        {
            count = count + 1;
            if (count -1 >= this.Directories.Count)
                return null;
            else
                return this.Directories[count-1];
        }
    }
}
