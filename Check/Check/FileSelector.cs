﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Check
{
    public partial class FileSelector : Form
    {
        public List<string> Result { get; set; }
        public FileSelector(string title, List<String> listOfFile)
        {
            InitializeComponent();
            lblTitre.Text = title;
            foreach(string s in listOfFile)
            {
                ListOfFileCLB.Items.Add(s, true);
            }            
        }

        private void FileSelector_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Result = new List<string>();
            foreach(string obj in ListOfFileCLB.CheckedItems)
                this.Result.Add(obj);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
