﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using Check;

public class TreeBuilder
{
    List<string> missingFile;
    public TreeBuilder(string[] items)
    {
        this.missingFile = new List<string>(items);
    }

    public void CreateTree(TreeView tree)
    {
        foreach (string element in missingFile)
        {
            List<string> file = new List<string>(element.Split('\\'));
            PopulateTreeView(tree.Nodes, element);
        }
        tree.ExpandAll();
    }
    private void PopulateTreeView(TreeNodeCollection nodes, string path)
    {
        CheckFile ck = new CheckFile(path);
        getSubFolder(nodes, ck);
    }
    public void getSubFolder(TreeNodeCollection nodes, CheckFile file)
    {
        string folder = file.getNextFolder();
        if (folder == null)
            nodes.Add(file.Name);
        else
        {
            TreeNode node = hasAlreadyNode(nodes, folder);
            if (node == null)
            {
                node = new TreeNode(folder);
                node.Checked = true;
                nodes.Add(node);
            }           
            getSubFolder(node.Nodes, file);
        }
    }
    public TreeNode hasAlreadyNode(TreeNodeCollection nodes, string node)
    {
        TreeNode res = null;
        foreach(TreeNode n in nodes)
        {
            if (n.Text == node)
                res = n;
        }
        return res;
    }
}