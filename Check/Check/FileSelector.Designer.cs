﻿namespace Check
{
    partial class FileSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListOfFileCLB = new System.Windows.Forms.CheckedListBox();
            this.lblTitre = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ListOfFileCLB
            // 
            this.ListOfFileCLB.FormattingEnabled = true;
            this.ListOfFileCLB.Location = new System.Drawing.Point(12, 29);
            this.ListOfFileCLB.Name = "ListOfFileCLB";
            this.ListOfFileCLB.Size = new System.Drawing.Size(776, 310);
            this.ListOfFileCLB.TabIndex = 0;
            // 
            // lblTitre
            // 
            this.lblTitre.AutoSize = true;
            this.lblTitre.Location = new System.Drawing.Point(12, 9);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(51, 17);
            this.lblTitre.TabIndex = 1;
            this.lblTitre.Text = "<Title>";
            // 
            // saveBtn
            // 
            this.saveBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.saveBtn.Location = new System.Drawing.Point(627, 345);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(161, 44);
            this.saveBtn.TabIndex = 2;
            this.saveBtn.Text = "Done !";
            this.saveBtn.UseVisualStyleBackColor = true;
            // 
            // FileSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.saveBtn;
            this.ClientSize = new System.Drawing.Size(800, 396);
            this.ControlBox = false;
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.lblTitre);
            this.Controls.Add(this.ListOfFileCLB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.MaximumSize = new System.Drawing.Size(818, 443);
            this.MinimumSize = new System.Drawing.Size(818, 443);
            this.Name = "FileSelector";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "File Selector";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FileSelector_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox ListOfFileCLB;
        private System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.Button saveBtn;
    }
}