﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Check
{
    public partial class Main : Form
    {
        private List<string> pathList;
        private List<string>[] resultList;
        public Main()
        {
            InitializeComponent();
        }
        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BtnPathOne_Click(object sender, EventArgs e)
        {
            BrowseHardDrive(TxtPathOne);
        }
        private void BtnPathTwo_Click(object sender, EventArgs e)
        {
            BrowseHardDrive(TxtPathTwo);
        }
        private void btnPathThree_Click(object sender, EventArgs e)
        {
            BrowseHardDrive(txtPathThree);
        }
        private void btnPathFour_Click(object sender, EventArgs e)
        {
            BrowseHardDrive(txtPathFour);
        }
        private void BrowseHardDrive(TextBox txt)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();
            txt.SelectedText=  fbd.SelectedPath;
        }
        private void BtnLaunch_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(TxtPathTwo.Text) || !Directory.Exists(TxtPathOne.Text))
                MessageBox.Show("One or both paths are invalid");
            else
            {
                pathList = new List<string>();
                pathList.Add(TxtPathTwo.Text);
                pathList.Add(TxtPathOne.Text);

                if (txtPathThree.Text != null && txtPathThree.Text != "")
                    pathList.Add(txtPathThree.Text);

                if (txtPathFour.Text != null && txtPathFour.Text != "")
                    pathList.Add(txtPathFour.Text);

                TreePathOne.Nodes.Clear();
                TreePathTwo.Nodes.Clear();
                TreePathThree.Nodes.Clear();
                TreePathFour.Nodes.Clear();

                CheckPaths(pathList);
                MessageBox.Show("Checking Files Done !");
                BtnCopy.Enabled = true;
            }            
        }
        private void BtnCopy_Click(object sender, EventArgs e)
        {
            List<string> result0, result1;
            //FileSelector selector0 =  new FileSelector("Files missing in the path : " +pathList[0], resultList[0]);
            FileSelector selector0 = new FileSelector("Files missing in the path : ",new List<string>());
            selector0.ShowDialog();
            result0 = selector0.Result;
            FileSelector selector1 = new FileSelector("Files missing in the path : " + pathList[1], resultList[1]);
            selector1.ShowDialog();
            result1 = selector1.Result;

            string savePath1 = pathList[1] + "\\CheckSave" + DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Month+"-"+DateTime.Now.Date.Year+"\\";
            string savePath0 = pathList[0] + "\\CheckSave" + DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Month+ "-" + DateTime.Now.Date.Year + "\\";
            if(!Directory.Exists(savePath0))
                Directory.CreateDirectory(savePath0);
            if(!Directory.Exists(savePath1))
                Directory.CreateDirectory(savePath1);   
            try
            {
                foreach (string file in result1)
                {
                    string[] splitFile = file.Split('\\');
                    string fileName = splitFile[splitFile.Length - 1];
                    string destFile = savePath0 + fileName;
                    File.Copy(file, destFile, true);
                }
                foreach (string file in result0)
                {
                    string[] splitFile = file.Split('\\');
                    string fileName = splitFile[splitFile.Length - 1];
                    string destFile = savePath1 + fileName;
                    File.Copy(file, destFile);
                }
            }
            catch(UnauthorizedAccessException execption){}
            MessageBox.Show("Copying Files Done !");
        }
        void CheckPaths(List<string> pathList)
        {
            List<string>[] fileOfPaths = new List<string>[pathList.Count];
            resultList = new List<string>[pathList.Count];
            for (int i = 0; i < pathList.Count; i++)
            {
                fileOfPaths[i] = GetAllFileOfPath(pathList[i]);
            }
            for(int i = 0; i< pathList.Count; i++)
            {
                for(int j = 0; j < pathList.Count; j++)
                {
                    if(j != i)
                    {
                        if (resultList[i] == null)
                            resultList[i] = new List<string>();
                        GetListOfMissingFIles(fileOfPaths[i], fileOfPaths[j], resultList[i]);
                    }                    
                }
                TreeView t = getTreeViewFromId(i);
                if(t != null)
                    new TreeBuilder(resultList[i].ToArray()).CreateTree(t);
            }
        }
        void GetListOfMissingFIles(List<String> pathToCompare, List<String> referencePath, List<String> res)
        {
            
            bool found;
            foreach (string fileToCompare in pathToCompare)
            {
                found = false;
                foreach (string path in referencePath)
                {
                    if (FileEquals(fileToCompare, path))
                    {
                        found = true;
                    }
                }
                if (!found)
                    res.Add(fileToCompare);
            }
        }
        TreeView getTreeViewFromId(int i )
        {
            switch(i)
            {
                case 1:
                    return TreePathOne;
                case 2:
                    return TreePathTwo;
                case 3:
                    return TreePathThree;
                case 4:
                    return TreePathFour;
                default:
                    return null;
            }
        }
        List<string> GetAllFileOfPath(string path)
        {
            List<String> res = new List<string>();
            try
            {
                foreach (string dir in Directory.GetDirectories(path))
                {
                    res.AddRange(GetAllFileOfPath(dir));
                }
                foreach (string file in Directory.GetFiles(path))
                {
                    res.Add(file);
                }
            }
            catch (UnauthorizedAccessException e) { }

           
            return res;
        }
        bool FileEquals(string pathToCompare, string path)
        {
            bool res = false;
            string nameOfFileToCompare = Path.GetFileName(pathToCompare);
            long sizeOfFileToCompare = new FileInfo(pathToCompare).Length;
            string nameOfFile = Path.GetFileName(path);
            long sizeOfFile = new FileInfo(path).Length;

            if (nameOfFileToCompare.Equals(nameOfFile) && sizeOfFile == sizeOfFileToCompare)
            {
                res = true;
            }

            return res;
        }

    }
}
