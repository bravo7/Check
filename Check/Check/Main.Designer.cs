﻿namespace Check
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnClose = new System.Windows.Forms.Button();
            this.checkTab = new System.Windows.Forms.TabControl();
            this.homePageTab = new System.Windows.Forms.TabPage();
            this.BtnCopy = new System.Windows.Forms.Button();
            this.LblPathTwo = new System.Windows.Forms.Label();
            this.LblPathOne = new System.Windows.Forms.Label();
            this.BtnLaunch = new System.Windows.Forms.Button();
            this.BtnPathTwo = new System.Windows.Forms.Button();
            this.BtnPathOne = new System.Windows.Forms.Button();
            this.TxtPathTwo = new System.Windows.Forms.TextBox();
            this.TxtPathOne = new System.Windows.Forms.TextBox();
            this.pathOnePageTab = new System.Windows.Forms.TabPage();
            this.pathTwoPageTab = new System.Windows.Forms.TabPage();
            this.pathThreePageTab = new System.Windows.Forms.TabPage();
            this.pathFourPageTab = new System.Windows.Forms.TabPage();
            this.supportPageTab = new System.Windows.Forms.TabPage();
            this.TreePathOne = new System.Windows.Forms.TreeView();
            this.TreePathTwo = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPathFour = new System.Windows.Forms.Button();
            this.btnPathThree = new System.Windows.Forms.Button();
            this.txtPathFour = new System.Windows.Forms.TextBox();
            this.txtPathThree = new System.Windows.Forms.TextBox();
            this.TreePathThree = new System.Windows.Forms.TreeView();
            this.TreePathFour = new System.Windows.Forms.TreeView();
            this.checkTab.SuspendLayout();
            this.homePageTab.SuspendLayout();
            this.pathOnePageTab.SuspendLayout();
            this.pathTwoPageTab.SuspendLayout();
            this.pathThreePageTab.SuspendLayout();
            this.pathFourPageTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnClose
            // 
            this.BtnClose.Location = new System.Drawing.Point(1261, 679);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(109, 32);
            this.BtnClose.TabIndex = 0;
            this.BtnClose.Text = "Close";
            this.BtnClose.UseVisualStyleBackColor = true;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // checkTab
            // 
            this.checkTab.Controls.Add(this.homePageTab);
            this.checkTab.Controls.Add(this.pathOnePageTab);
            this.checkTab.Controls.Add(this.pathTwoPageTab);
            this.checkTab.Controls.Add(this.pathThreePageTab);
            this.checkTab.Controls.Add(this.pathFourPageTab);
            this.checkTab.Controls.Add(this.supportPageTab);
            this.checkTab.Location = new System.Drawing.Point(12, 12);
            this.checkTab.Name = "checkTab";
            this.checkTab.SelectedIndex = 0;
            this.checkTab.Size = new System.Drawing.Size(1358, 661);
            this.checkTab.TabIndex = 17;
            // 
            // homePageTab
            // 
            this.homePageTab.Controls.Add(this.label1);
            this.homePageTab.Controls.Add(this.label2);
            this.homePageTab.Controls.Add(this.btnPathFour);
            this.homePageTab.Controls.Add(this.btnPathThree);
            this.homePageTab.Controls.Add(this.txtPathFour);
            this.homePageTab.Controls.Add(this.txtPathThree);
            this.homePageTab.Controls.Add(this.BtnCopy);
            this.homePageTab.Controls.Add(this.LblPathTwo);
            this.homePageTab.Controls.Add(this.LblPathOne);
            this.homePageTab.Controls.Add(this.BtnLaunch);
            this.homePageTab.Controls.Add(this.BtnPathTwo);
            this.homePageTab.Controls.Add(this.BtnPathOne);
            this.homePageTab.Controls.Add(this.TxtPathTwo);
            this.homePageTab.Controls.Add(this.TxtPathOne);
            this.homePageTab.Location = new System.Drawing.Point(4, 25);
            this.homePageTab.Name = "homePageTab";
            this.homePageTab.Padding = new System.Windows.Forms.Padding(3);
            this.homePageTab.Size = new System.Drawing.Size(1350, 632);
            this.homePageTab.TabIndex = 0;
            this.homePageTab.Text = "Home";
            this.homePageTab.UseVisualStyleBackColor = true;
            // 
            // BtnCopy
            // 
            this.BtnCopy.Location = new System.Drawing.Point(1006, 274);
            this.BtnCopy.Name = "BtnCopy";
            this.BtnCopy.Size = new System.Drawing.Size(109, 32);
            this.BtnCopy.TabIndex = 24;
            this.BtnCopy.Text = "Copy";
            this.BtnCopy.UseVisualStyleBackColor = true;
            this.BtnCopy.Click += new System.EventHandler(this.BtnCopy_Click);
            // 
            // LblPathTwo
            // 
            this.LblPathTwo.AutoSize = true;
            this.LblPathTwo.Location = new System.Drawing.Point(33, 89);
            this.LblPathTwo.Name = "LblPathTwo";
            this.LblPathTwo.Size = new System.Drawing.Size(57, 17);
            this.LblPathTwo.TabIndex = 23;
            this.LblPathTwo.Text = "Path 2 :";
            // 
            // LblPathOne
            // 
            this.LblPathOne.AutoSize = true;
            this.LblPathOne.Location = new System.Drawing.Point(33, 40);
            this.LblPathOne.Name = "LblPathOne";
            this.LblPathOne.Size = new System.Drawing.Size(57, 17);
            this.LblPathOne.TabIndex = 22;
            this.LblPathOne.Text = "Path 1 :";
            // 
            // BtnLaunch
            // 
            this.BtnLaunch.Location = new System.Drawing.Point(1134, 274);
            this.BtnLaunch.Name = "BtnLaunch";
            this.BtnLaunch.Size = new System.Drawing.Size(109, 32);
            this.BtnLaunch.TabIndex = 21;
            this.BtnLaunch.Text = "Launch";
            this.BtnLaunch.UseVisualStyleBackColor = true;
            this.BtnLaunch.Click += new System.EventHandler(this.BtnLaunch_Click);
            // 
            // BtnPathTwo
            // 
            this.BtnPathTwo.Location = new System.Drawing.Point(1134, 84);
            this.BtnPathTwo.Name = "BtnPathTwo";
            this.BtnPathTwo.Size = new System.Drawing.Size(109, 32);
            this.BtnPathTwo.TabIndex = 20;
            this.BtnPathTwo.Text = "Browse";
            this.BtnPathTwo.UseVisualStyleBackColor = true;
            this.BtnPathTwo.Click += new System.EventHandler(this.BtnPathTwo_Click);
            // 
            // BtnPathOne
            // 
            this.BtnPathOne.Location = new System.Drawing.Point(1134, 35);
            this.BtnPathOne.Name = "BtnPathOne";
            this.BtnPathOne.Size = new System.Drawing.Size(109, 32);
            this.BtnPathOne.TabIndex = 19;
            this.BtnPathOne.Text = "Browse";
            this.BtnPathOne.UseVisualStyleBackColor = true;
            this.BtnPathOne.Click += new System.EventHandler(this.BtnPathOne_Click);
            // 
            // TxtPathTwo
            // 
            this.TxtPathTwo.Location = new System.Drawing.Point(96, 89);
            this.TxtPathTwo.Name = "TxtPathTwo";
            this.TxtPathTwo.Size = new System.Drawing.Size(1019, 22);
            this.TxtPathTwo.TabIndex = 18;
            // 
            // TxtPathOne
            // 
            this.TxtPathOne.Location = new System.Drawing.Point(96, 40);
            this.TxtPathOne.Name = "TxtPathOne";
            this.TxtPathOne.Size = new System.Drawing.Size(1019, 22);
            this.TxtPathOne.TabIndex = 17;
            // 
            // pathOnePageTab
            // 
            this.pathOnePageTab.Controls.Add(this.TreePathOne);
            this.pathOnePageTab.Location = new System.Drawing.Point(4, 25);
            this.pathOnePageTab.Name = "pathOnePageTab";
            this.pathOnePageTab.Padding = new System.Windows.Forms.Padding(3);
            this.pathOnePageTab.Size = new System.Drawing.Size(1350, 632);
            this.pathOnePageTab.TabIndex = 1;
            this.pathOnePageTab.Text = "Path 1";
            this.pathOnePageTab.UseVisualStyleBackColor = true;
            // 
            // pathTwoPageTab
            // 
            this.pathTwoPageTab.Controls.Add(this.TreePathTwo);
            this.pathTwoPageTab.Location = new System.Drawing.Point(4, 25);
            this.pathTwoPageTab.Name = "pathTwoPageTab";
            this.pathTwoPageTab.Padding = new System.Windows.Forms.Padding(3);
            this.pathTwoPageTab.Size = new System.Drawing.Size(1350, 632);
            this.pathTwoPageTab.TabIndex = 2;
            this.pathTwoPageTab.Text = "Path 2";
            this.pathTwoPageTab.UseVisualStyleBackColor = true;
            // 
            // pathThreePageTab
            // 
            this.pathThreePageTab.Controls.Add(this.TreePathThree);
            this.pathThreePageTab.Location = new System.Drawing.Point(4, 25);
            this.pathThreePageTab.Name = "pathThreePageTab";
            this.pathThreePageTab.Padding = new System.Windows.Forms.Padding(3);
            this.pathThreePageTab.Size = new System.Drawing.Size(1350, 632);
            this.pathThreePageTab.TabIndex = 3;
            this.pathThreePageTab.Text = "Path 3";
            this.pathThreePageTab.UseVisualStyleBackColor = true;
            // 
            // pathFourPageTab
            // 
            this.pathFourPageTab.Controls.Add(this.TreePathFour);
            this.pathFourPageTab.Location = new System.Drawing.Point(4, 25);
            this.pathFourPageTab.Name = "pathFourPageTab";
            this.pathFourPageTab.Padding = new System.Windows.Forms.Padding(3);
            this.pathFourPageTab.Size = new System.Drawing.Size(1350, 632);
            this.pathFourPageTab.TabIndex = 4;
            this.pathFourPageTab.Text = "Path 4";
            this.pathFourPageTab.UseVisualStyleBackColor = true;
            // 
            // supportPageTab
            // 
            this.supportPageTab.Location = new System.Drawing.Point(4, 25);
            this.supportPageTab.Name = "supportPageTab";
            this.supportPageTab.Padding = new System.Windows.Forms.Padding(3);
            this.supportPageTab.Size = new System.Drawing.Size(817, 430);
            this.supportPageTab.TabIndex = 5;
            this.supportPageTab.Text = "Support";
            this.supportPageTab.UseVisualStyleBackColor = true;
            // 
            // TreePathOne
            // 
            this.TreePathOne.Location = new System.Drawing.Point(6, 6);
            this.TreePathOne.Name = "TreePathOne";
            this.TreePathOne.Size = new System.Drawing.Size(1338, 620);
            this.TreePathOne.TabIndex = 15;
            // 
            // TreePathTwo
            // 
            this.TreePathTwo.Location = new System.Drawing.Point(6, 6);
            this.TreePathTwo.Name = "TreePathTwo";
            this.TreePathTwo.Size = new System.Drawing.Size(1338, 620);
            this.TreePathTwo.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 30;
            this.label1.Text = "Path 4 :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 29;
            this.label2.Text = "Path 3 :";
            // 
            // btnPathFour
            // 
            this.btnPathFour.Location = new System.Drawing.Point(1134, 181);
            this.btnPathFour.Name = "btnPathFour";
            this.btnPathFour.Size = new System.Drawing.Size(109, 32);
            this.btnPathFour.TabIndex = 28;
            this.btnPathFour.Text = "Browse";
            this.btnPathFour.UseVisualStyleBackColor = true;
            this.btnPathFour.Click += new System.EventHandler(this.btnPathFour_Click);
            // 
            // btnPathThree
            // 
            this.btnPathThree.Location = new System.Drawing.Point(1134, 132);
            this.btnPathThree.Name = "btnPathThree";
            this.btnPathThree.Size = new System.Drawing.Size(109, 32);
            this.btnPathThree.TabIndex = 27;
            this.btnPathThree.Text = "Browse";
            this.btnPathThree.UseVisualStyleBackColor = true;
            this.btnPathThree.Click += new System.EventHandler(this.btnPathThree_Click);
            // 
            // txtPathFour
            // 
            this.txtPathFour.Location = new System.Drawing.Point(96, 186);
            this.txtPathFour.Name = "txtPathFour";
            this.txtPathFour.Size = new System.Drawing.Size(1019, 22);
            this.txtPathFour.TabIndex = 26;
            // 
            // txtPathThree
            // 
            this.txtPathThree.Location = new System.Drawing.Point(96, 137);
            this.txtPathThree.Name = "txtPathThree";
            this.txtPathThree.Size = new System.Drawing.Size(1019, 22);
            this.txtPathThree.TabIndex = 25;
            // 
            // TreePathThree
            // 
            this.TreePathThree.Location = new System.Drawing.Point(6, 6);
            this.TreePathThree.Name = "TreePathThree";
            this.TreePathThree.Size = new System.Drawing.Size(1338, 620);
            this.TreePathThree.TabIndex = 17;
            // 
            // TreePathFour
            // 
            this.TreePathFour.Location = new System.Drawing.Point(6, 6);
            this.TreePathFour.Name = "TreePathFour";
            this.TreePathFour.Size = new System.Drawing.Size(1338, 620);
            this.TreePathFour.TabIndex = 17;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1382, 723);
            this.ControlBox = false;
            this.Controls.Add(this.checkTab);
            this.Controls.Add(this.BtnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.MaximumSize = new System.Drawing.Size(1400, 770);
            this.MinimumSize = new System.Drawing.Size(1400, 770);
            this.Name = "Main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Check";
            this.checkTab.ResumeLayout(false);
            this.homePageTab.ResumeLayout(false);
            this.homePageTab.PerformLayout();
            this.pathOnePageTab.ResumeLayout(false);
            this.pathTwoPageTab.ResumeLayout(false);
            this.pathThreePageTab.ResumeLayout(false);
            this.pathFourPageTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button BtnClose;
        private System.Windows.Forms.TabControl checkTab;
        private System.Windows.Forms.TabPage homePageTab;
        private System.Windows.Forms.Button BtnCopy;
        private System.Windows.Forms.Label LblPathTwo;
        private System.Windows.Forms.Label LblPathOne;
        private System.Windows.Forms.Button BtnLaunch;
        private System.Windows.Forms.Button BtnPathTwo;
        private System.Windows.Forms.Button BtnPathOne;
        private System.Windows.Forms.TextBox TxtPathTwo;
        private System.Windows.Forms.TextBox TxtPathOne;
        private System.Windows.Forms.TabPage pathOnePageTab;
        private System.Windows.Forms.TreeView TreePathOne;
        private System.Windows.Forms.TabPage pathTwoPageTab;
        private System.Windows.Forms.TreeView TreePathTwo;
        private System.Windows.Forms.TabPage pathThreePageTab;
        private System.Windows.Forms.TabPage pathFourPageTab;
        private System.Windows.Forms.TabPage supportPageTab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPathFour;
        private System.Windows.Forms.Button btnPathThree;
        private System.Windows.Forms.TextBox txtPathFour;
        private System.Windows.Forms.TextBox txtPathThree;
        private System.Windows.Forms.TreeView TreePathThree;
        private System.Windows.Forms.TreeView TreePathFour;
    }
}